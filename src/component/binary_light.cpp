#include <memory>

#include "ros/ros.h"
#include "std_msgs/Bool.h"
#include "polyorbite_rover_hardware_library/component/BinaryLight.h"

#define STATE_TOPIC_NAME "state"
#define STATE_TOPIC_QUEUE_SIZE 10

#define LIGHT_INITIAL_VALUE_PARAM_NAME "initial_value"
#define LIGHT_PIN_INDEX_PARAM_NAME "pin_index"

std::unique_ptr<BinaryLight> binaryLightPtr;

void updateLightState(const std_msgs::Bool::ConstPtr& message)
{
    bool shouldBeOn = message->data;

    if(shouldBeOn)
        binaryLightPtr->turnOn();
    else
        binaryLightPtr->turnOff();
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "rover_binary_light");

    ros::NodeHandle nodeHandle;

    bool lightInitialValue;
    int lightPinIndex;
    nodeHandle.param<bool>(LIGHT_INITIAL_VALUE_PARAM_NAME, lightInitialValue);
    nodeHandle.param<int>(LIGHT_PIN_INDEX_PARAM_NAME, lightPinIndex);

    binaryLightPtr = std::make_unique<BinaryLight>(
        lightInitialValue,
        lightPinIndex
    );

    ros::Subscriber stateSubscriber = nodeHandle.subscribe(
        STATE_TOPIC_NAME,
        STATE_TOPIC_QUEUE_SIZE,
        updateLightState
    );

    ros::spin();
}