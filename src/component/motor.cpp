#include <stdint.h>
#include <memory>

#include "ros/ros.h"
#include "std_msgs/Float32.h"
#include "polyorbite_rover_hardware_library/component/Motor.h"

#define DESIRED_VELOCITY_TOPIC_NAME "desired_velocity"
#define DESIRED_VELOCITY_TOPIC_QUEUE_SIZE 10

#define DIRECTION_PIN_INDEX_PARAM_NAME "direction_pin_index"
#define DIRECTION_PIN_FORWARD_VALUE_PARAM_NAME "direction_pin_forward_value"
#define PWM_PIN_INDEX_PARAM_NAME "pwm_pin_index"

std::unique_ptr<Motor> motorPtr;

void onDesiredVelocityReceived(const std_msgs::Float32::ConstPtr& message)
{
    motorPtr->setVelocity(message->data);
}

int main(int argc, char** argv)
{    
    ros::init(argc, argv, "rover_motor");

    ros::NodeHandle nodeHandle;
    ros::NodeHandle privateNodeHandle("~");

    int directionPinIndex;
    bool directionPinForwardValue;
    int pwmPinIndex;
    privateNodeHandle.param<int>(DIRECTION_PIN_INDEX_PARAM_NAME, directionPinIndex);
    privateNodeHandle.param<bool>(DIRECTION_PIN_FORWARD_VALUE_PARAM_NAME, directionPinForwardValue);
    privateNodeHandle.param<int>(PWM_PIN_INDEX_PARAM_NAME, pwmPinIndex);

    motorPtr = std::make_unique<Motor>(
        directionPinIndex,
        directionPinForwardValue,
        pwmPinIndex
    );

    ros::Subscriber desiredVelocitySubscriber = nodeHandle.subscribe(
        DESIRED_VELOCITY_TOPIC_NAME,
        DESIRED_VELOCITY_TOPIC_QUEUE_SIZE,
        onDesiredVelocityReceived
    );

    ros::spin();
}